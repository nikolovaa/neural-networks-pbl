These two files contain the code, with which I trained the 
Artificial Neural Network models, evaluated them and then 
plotted their performance values. The "Artificial_Neural_Network" 
file contains the implementation of a model with Embedding and Bi_LSTM layers.
In the other file multiple models are implemented to examine
the influence of increasing the number of layers and 
the change of the activation function on the performance of the model. 

